{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Servant.HttpRpc.Client
  (
  ) where

import           Control.Monad (unless)
import           Data.Foldable (toList)
import           Data.Proxy (Proxy (..))
import qualified Data.Sequence as Sequence
import qualified Data.Text as T
import           Network.HTTP.Media (MediaType, matches, parseAccept, (//))
import           Network.HTTP.Types (methodPost)

import           Servant.API.ContentTypes
    (MimeUnrender, contentTypes, mimeUnrender)
import           Servant.Client.Core

import           Servant.HttpRpc.API


instance
  ( RunClient m
  , cts' ~ (ct ': cts)
  , r ~ RpcInputType es a format
  , RpcParseFormat es a format
  , MimeUnrender ct r
  )
  => HasClient m (RPC cts' format es a) where
  type Client m (RPC cts' format es a) = m (Either es a)

  clientWithRoute _ _ req = do
    response <- runRequestAcceptStatus Nothing req
      { requestMethod = methodPost
      , requestAccept = Sequence.fromList $ toList accept
      }
    decoded <- response `decodedAs` (Proxy :: Proxy ct)
    pure $! parseRpcOutput (Proxy :: Proxy format) (Proxy :: Proxy es) decoded
    where
      accept = contentTypes (Proxy :: Proxy ct)

  hoistClientMonad _ _ f = f

-- | Copied from servant-client-core
checkContentTypeHeader :: RunClient m => Response -> m MediaType
checkContentTypeHeader response =
  case lookup "Content-Type" $ toList $ responseHeaders response of
    Nothing -> return $ "application"//"octet-stream"
    Just t -> case parseAccept t of
      Nothing -> throwClientError $ InvalidContentTypeHeader response
      Just t' -> return t'


-- | Copied from servant-client-core
decodedAs :: forall ct a m. (MimeUnrender ct a, RunClient m)
  => Response -> Proxy ct -> m a
decodedAs response ct = do
  responseContentType <- checkContentTypeHeader response
  unless (any (matches responseContentType) accept) $
    throwClientError $ UnsupportedContentType responseContentType response
  case mimeUnrender ct $ responseBody response of
    Left err -> throwClientError $ DecodeFailure (T.pack err) response
    Right val -> return val
  where
    accept = toList $ contentTypes ct
