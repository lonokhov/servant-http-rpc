let
  overrides = import ./packages.nix;
  base = import (import ./pkgs.nix) { overlays = [overrides];};
in
  base.haskellPackages.shellFor {
    packages = p: [
      p.servant-http-rpc-api
      p.servant-http-rpc-server
      p.servant-http-rpc-client
      p.servant-http-rpc-swagger
    ];
    buildInputs = [
      base.bashInteractive
    ];
    withHoogle = true;
    shellHook = ''
      unset preHook
      rm -f ${toString ./.}/.ghc.environment.*
      export CABAL_DIR=${builtins.toString ./.cabal}
    '';
  }
