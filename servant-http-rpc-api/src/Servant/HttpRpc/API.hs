{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}
module Servant.HttpRpc.API
    ( RPC
    , JsonRpc
    , RpcOutputFormat(..)
    , RpcParseFormat(..)
    , RpcResponse(..)
    , RpcSimpleFormat
    , module M
    ) where
import           Control.Applicative ((<|>))
import           Data.Aeson (FromJSON (..), ToJSON (..))
import qualified Data.Aeson as J
import           Data.Kind (Type)
import           Data.Proxy (Proxy)
import           Servant.API.ContentTypes (JSON)

import           Servant.HttpRpc.API.RpcErrors as M

data RPC (contentTypes :: [Type]) (format :: Type) (e :: Type) a

type JsonRpc e r = RPC '[JSON] RpcSimpleFormat e r

class RpcOutputFormat (es :: Type) r (a :: Type) where
  type RpcOutputType es r a :: Type
  toRpcOutput
    :: Proxy a
    -> Proxy es
    -> Either es r
    -> RpcOutputType es r a

class RpcParseFormat (es :: Type) r (a :: Type) where
  type RpcInputType es r a :: Type
  parseRpcOutput
    :: Proxy a
    -> Proxy es
    -> RpcInputType es r a
    -> Either es r

data RpcSimpleFormat

data RpcResponse e a
  = RpcResponseOk a
  | RpcResponseError e
  deriving (Show)

instance (ToJSON e, ToJSON a) => ToJSON (RpcResponse e a) where
  toJSON = \case
    RpcResponseOk a -> J.object ["result" J..= a]
    RpcResponseError e -> J.object ["error" J..= e]
  {-# INLINE toJSON #-}
  toEncoding = \case
    RpcResponseOk a -> J.pairs $ "result" J..= a
    RpcResponseError e -> J.pairs $ "error" J..= e
  {-# INLINE toEncoding #-}

instance (FromJSON e, FromJSON a) => FromJSON (RpcResponse e a) where
  parseJSON = J.withObject "RpcResponse" $ \o -> do
    v <- Right <$> (o J..: "result") <|> Left <$> (o J..: "error")
    case v of
      Right r -> RpcResponseOk <$> parseJSON r
      Left l -> RpcResponseError <$> parseJSON l
  {-# INLINE parseJSON #-}

instance RpcOutputFormat e a RpcSimpleFormat where
  type RpcOutputType e a RpcSimpleFormat = RpcResponse e a
  toRpcOutput _ _ = either RpcResponseError RpcResponseOk

instance RpcParseFormat e a RpcSimpleFormat where
  type RpcInputType e a RpcSimpleFormat = RpcResponse e a
  parseRpcOutput _ _ = \case
    RpcResponseError e -> Left e
    RpcResponseOk r -> Right r
