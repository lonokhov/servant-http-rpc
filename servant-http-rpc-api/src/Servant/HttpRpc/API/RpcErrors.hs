{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}
module Servant.HttpRpc.API.RpcErrors
    ( injectError
    , RpcErrors (..)
    , Map
    , Sum
    , ToException1
    , rpcErrorsToException
    , SumFromJson
    , SumToJson
    ) where

import           Control.Applicative ((<|>))
import           Control.Exception
import           Data.Aeson (FromJSON (..), ToJSON (..))
import qualified Data.Aeson as J
import qualified Data.Aeson.Types as J
import           Data.Functor.Classes (Show1)
import           Data.Functor.Const (Const (..))
import           Data.Kind (Type)
import           Data.Proxy (Proxy (..))
import           Data.Sum (Apply (..), ElemIndex, Sum, inject, (:<))
import           Data.Void (Void)
import           GHC.TypeNats (KnownNat)

type family Map (f :: Type -> Type -> Type) (xs :: [Type]) where
  Map f '[]       = '[]
  Map f (x ': xs) = f x ': Map f xs

newtype RpcErrors es = RpcErrors
  { unRpcErrors :: Sum (Map Const es) Void }

deriving instance Apply Show1 (Map Const es) => Show (RpcErrors es)

injectError :: (Const e :< r) => r ~ Map Const es => e -> RpcErrors es
injectError = RpcErrors . inject . Const

class ToException1 f where
  toException1 :: f a -> SomeException

instance Exception a => ToException1 (Const a) where
  toException1 = toException . getConst

instance Apply ToException1 es => ToException1 (Sum es) where
  toException1 = apply @ToException1 toException1


rpcErrorsToException
  :: Apply ToException1 (Map Const es)
  => RpcErrors es
  -> SomeException
rpcErrorsToException (RpcErrors es) = toException1 es


class SumToJson f where
  sumToJSON :: f a -> J.Value
  sumToEncoding :: f a -> J.Encoding

instance ToJSON a => SumToJson (Const a) where
  sumToJSON = toJSON . getConst
  sumToEncoding = toEncoding . getConst

instance Apply SumToJson es => SumToJson (Sum es) where
  sumToJSON = apply @SumToJson sumToJSON
  sumToEncoding = apply @SumToJson sumToEncoding

instance Apply SumToJson (Map Const es) => ToJSON (RpcErrors es) where
  toJSON (RpcErrors es) = sumToJSON es
  toEncoding (RpcErrors es) = sumToEncoding es


class SumFromJson (es :: [Type]) (e :: [Type]) where
  sumParseJSON :: Proxy es -> Proxy e -> J.Value -> J.Parser (RpcErrors es)

instance SumFromJson u '[] where
  sumParseJSON _ _ _ = fail "Can't parse sum"

instance forall as xs a .
  ( FromJSON a
  , SumFromJson as xs
  , KnownNat (ElemIndex (Const a) (Map Const as))
  ) => (SumFromJson as (a ': xs)) where
  sumParseJSON pa _ v =
    injectError <$> (parseJSON v :: J.Parser a)
    <|> sumParseJSON pa (Proxy @xs) v

instance SumFromJson es es => FromJSON (RpcErrors es) where
  parseJSON = sumParseJSON (Proxy @es)  (Proxy @es)

