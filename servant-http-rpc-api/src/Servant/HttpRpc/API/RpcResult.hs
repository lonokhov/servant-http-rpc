{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE StandaloneDeriving    #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}
module Servant.HttpRpc.API.RpcResult
    ( RpcResult (..)
    , RpcResultFormat
    ) where

import           Control.Applicative ((<|>))
import           Data.Aeson (FromJSON (..), ToJSON (..))
import qualified Data.Aeson as J
import           Data.Functor.Classes (Show1)
import           Data.Functor.Const (Const (..))
import           Data.Sum (Apply (..))

import           Servant.HttpRpc.API (RpcOutputFormat (..), RpcParseFormat (..))

import           Servant.HttpRpc.API.RpcErrors
    (Map, RpcErrors (..), SumFromJson)

data RpcResultFormat

data RpcResult es a = RpcError (RpcErrors es)  | RpcOk a

deriving instance (Apply Show1 (Map Const es), Show a) => Show (RpcResult es a)

instance RpcOutputFormat (RpcErrors es) r RpcResultFormat where
  type RpcOutputType (RpcErrors es) r RpcResultFormat = RpcResult es r

  toRpcOutput _ _ = either RpcError RpcOk

instance RpcParseFormat (RpcErrors es) r RpcResultFormat where
  type RpcInputType (RpcErrors es) r RpcResultFormat = RpcResult es r

  parseRpcOutput _ _ = \case
    RpcOk r -> Right r
    RpcError r -> Left r

instance
  ( ToJSON (RpcErrors es)
  , ToJSON a)
  => ToJSON (RpcResult es a) where
  toJSON r = J.object . (:[]) $ case r of
    RpcOk o    -> "result" J..= o
    RpcError e -> "error" J..= e

  toEncoding r = J.pairs $ case r of
    RpcOk o    -> "result" J..= o
    RpcError e -> "error" J..= e

instance (FromJSON a, SumFromJson es es)
  => FromJSON (RpcResult es a) where
  parseJSON = J.withObject "RpcResult" $ \o ->
    parseResult o <|> parseErrors o
    where
      parseResult o =
        RpcOk <$> (o J..: "result")
      parseErrors o = do
        e <- o J..: "errors"
        RpcError <$> parseJSON e
