# Revision history for servant-http-rpc-api

## 0.1.2.0 -- 2019-09-19

* Allow servant 0.16

## 0.1.0.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.
