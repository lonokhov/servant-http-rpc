{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TupleSections         #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Servant.HttpRpc.Server
    (
    ) where

import qualified Data.ByteString.Lazy as LB
import           Data.Either (isRight)
import           Data.Kind (Type)
import           Data.Maybe (fromMaybe)
import           Data.Proxy (Proxy (..))
import           Network.HTTP.Types
    (Header, badRequest400, hAccept, hContentType, methodPost, ok200)

import qualified Network.Wai as Wai
import           Servant.API (Headers, JSON)
import           Servant.API.ContentTypes (AcceptHeader (..), AllCTRender (..))
import           Servant.API.ResponseHeaders
    (GetHeaders, getHeaders, getResponse)
import           Servant.Auth.Server.Internal.AddSetCookie
    (AddSetCookieApi, AddSetCookieApiVerb)

import           Servant.Server (Handler, HasServer (..), err409)
import           Servant.Server.Internal
    (Delayed, RouteResult (..), Router, acceptCheck, addAcceptCheck,
    addMethodCheck, allowedMethodHead, ct_wildcard, leafRouter, methodCheck,
    runAction)

import           Servant.HttpRpc.API (RPC, RpcOutputFormat (..))

instance {-# OVERLAPPABLE #-}
  ( RpcOutputFormat es a fmt
  , AllCTRender ctypes (RpcOutputType es a fmt)
  ) => HasServer (RPC ctypes fmt es a) ctx where
  type ServerT (RPC ctypes fmt es a) m = m (Either es a)
  hoistServerWithContext _ _ nt = nt
  route Proxy _  =
    methodRouter (Proxy @es) (Proxy @fmt) (Proxy @ctypes) ([],)

instance {-# OVERLAPPING #-}
  ( RpcOutputFormat es a fmt
  , AllCTRender ctypes (RpcOutputType es a fmt)
  , GetHeaders (Headers h a)
  ) => HasServer (RPC ctypes fmt es (Headers h a)) ctx where
  type ServerT (RPC ctypes fmt es (Headers h a)) m =
    m (Either es (Headers h a))
  hoistServerWithContext _ _ nt = nt
  route Proxy _  =
    methodRouter (Proxy @es) (Proxy @fmt) (Proxy @ctypes)
    $ \case
        Left e -> ([], Left e)
        Right r -> (getHeaders r, Right (getResponse r))

methodRouter
  :: RpcOutputFormat es b format
  => (AllCTRender ctypes (RpcOutputType es b format))
  => Proxy (es :: Type)
  -> Proxy format
  -> Proxy (ctypes :: [Type])
  -> (a -> ([Header], Either es b))
  -> Delayed env (Handler a)
  -> Router env
methodRouter perrs pformat pctypes splitHeaders action = leafRouter route'
  where
    proxy = Proxy :: Proxy '[JSON]
    route' env request respond =
      let
        accH = AcceptHeader
               . fromMaybe ct_wildcard
               . lookup hAccept
               $ Wai.requestHeaders request
      in runAction (action `addMethodCheck` methodCheck methodPost request
                           `addAcceptCheck` acceptCheck proxy accH
                   ) env request respond $ \output -> do
      let
        (headers, result) = splitHeaders output
        code = if isRight result then ok200 else badRequest400
        formated = toRpcOutput pformat perrs result
      case handleAcceptH pctypes accH formated of
        Nothing -> FailFatal err409
        Just (cType, body) ->
          let
            respHeaders = (hContentType, LB.toStrict cType):headers
            bdy = if allowedMethodHead methodPost request then mempty else body
          in Route $ Wai.responseLBS code respHeaders bdy


type instance AddSetCookieApi (RPC ct fmt e a) =
  RPC ct fmt e (AddSetCookieApiVerb a)
