# Revision history for servant-http-rpc-server

## 0.1.1.0 -- 2019-09-19

* Allow servant 0.16

## 0.1.0.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.
