self : super:
let
  lib = super.haskell.lib;

  haskellSrcFilter = name: type:
    let base = builtins.baseNameOf name;
     in super.lib.cleanSourceFilter name type &&
       (type != "directory" || (base != "dist" && base != "dist-newstyle"));

  filterHaskell = builtins.filterSource haskellSrcFilter;

  callCabalWith = ghc: name: path: args:
     let
       d = ghc.callCabal2nix name path args;
     in lib.overrideCabal d (_ : { src = filterHaskell d.src; } );

  haskellOverrides = hself: hsuper:
    let
      callCabal = callCabalWith hself;
    in {
      servant-http-rpc-api =
        callCabal "servant-http-rpc-api"
        ./servant-http-rpc-api
        {};
      servant-http-rpc-server =
        callCabal "servant-http-rpc-server"
        ./servant-http-rpc-server
        {};
      servant-http-rpc-swagger =
        callCabal "servant-http-rpc-swagger"
        ./servant-http-rpc-swagger
        {};
      servant-http-rpc-client =
        callCabal "servant-http-rpc-client"
        ./servant-http-rpc-client
        {};
    };
in rec {
  haskell = super.haskell // {
    packages = super.haskell.packages // {
      ghc865 = super.haskell.packages.ghc865.override {
        overrides = haskellOverrides;
      };
    };
  };
  haskellPackages = haskell.packages.ghc865;
}
