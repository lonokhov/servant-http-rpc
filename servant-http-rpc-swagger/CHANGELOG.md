# Revision history for servant-http-rpc-swagger

## 0.2.0.0 -- 2019-09-19

* Bump swagger to 2.4 and allow servant 0.16

## 0.1.0.0 -- YYYY-mm-dd

* First version. Released on an unsuspecting world.
