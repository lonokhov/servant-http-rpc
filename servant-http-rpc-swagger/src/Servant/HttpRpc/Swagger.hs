{-# LANGUAGE BangPatterns          #-}
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLists       #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE PolyKinds             #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
module Servant.HttpRpc.Swagger
  (
  ) where


import           Control.Lens
    (_2, _3, at, folded, to, view, (%~), (&), (.~), (<&>), (?~), (^..))
import           Data.Aeson (ToJSON (..), Value)
import           Data.Aeson.Encode.Pretty (encodePrettyToTextBuilder)
import           Data.Kind (Type)
import           Data.Proxy (Proxy (..))
import           Data.Swagger
    (MimeList (..), NamedSchema (..), Referenced (..), Schema, SwaggerType (..),
    ToSchema (..), declareSchemaRef, definitions, description, enum_,
    paramSchema, paths, post, produces, properties, required, schema, type_)
import           Data.Swagger.Declare (runDeclare)
import           Data.Text (Text)
import qualified Data.Text as Text
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Builder as BL

import           Servant.Docs (ToSample (..))
import           Servant.Swagger (HasSwagger (..), toSwagger)
import           Servant.Swagger.Internal (AllAccept (..))

import           Servant.HttpRpc.API
    (RPC, RpcErrors, RpcOutputFormat (..), injectError)

class AllErrSchemas fmt (es :: [Type]) (e :: [Type]) where
  allErrSchemas
    :: Proxy fmt
    -> Proxy es
    -> Proxy e
    -> [(Schema, [Value], [(Text, Value)])]


instance AllErrSchemas fmt es '[] where
  allErrSchemas _ _ _ = []

instance
  forall e as es fmt r .
  ( ToSample e
  , ToSchema e
  , ToJSON e
  , AllErrSchemas fmt as es
  , RpcOutputFormat (RpcErrors '[e]) () fmt
  , r ~ RpcOutputType (RpcErrors '[e]) () fmt
  , ToJSON r
  ) => AllErrSchemas fmt as (e:es) where
  allErrSchemas pfmt pall _ =
    (declaredSchema, enums, errSamples) : allErrSchemas pfmt pall (Proxy @es)
    where
      samples = toSamples (Proxy :: Proxy e)
      enums = samples ^.. folded._2 . to toJSON
      toErr e = toRpcOutput (Proxy @fmt) (Proxy @(RpcErrors '[e]))
                (Left ( injectError e) :: Either (RpcErrors '[e]) ())
      !errSamples = samples <&> _2 %~ toJSON . toErr

      (_, NamedSchema _ !declaredSchema) =
        runDeclare (declareNamedSchema (Proxy @e)) mempty

instance
  forall (es :: [Type]) a fmt r ctypes .
  ( RpcOutputFormat (RpcErrors es) a fmt
  , r ~ RpcOutputType (RpcErrors es) a fmt
  , AllErrSchemas fmt es es
  , ToSchema r
  , ToSchema (RpcOutputType (RpcErrors es) a fmt)
  , AllAccept ctypes
  ) => HasSwagger (RPC ctypes fmt (RpcErrors es) a) where
 toSwagger _ = mempty
    & definitions .~ def
    & paths.at "/" ?~
      (mempty & post ?~ (mempty
              & produces ?~ MimeList ["application/json"]
              & at 200 ?~ Inline (mempty
                 & schema ?~ Inline sh
              )
              & at 400 ?~ Inline (mempty
                 & schema ?~ Inline err
                 & description .~ examples
              )))
    where
      (def, ref) = flip runDeclare mempty
        . declareSchemaRef $ Proxy @(RpcOutputType (RpcErrors es) a fmt)
      sh = mempty
        & type_ ?~ SwaggerObject
        & properties .~ [("result", ref)]
        & required .~ ["result"]

      err = mempty
        & type_ ?~ SwaggerObject
        & properties .~ [("error", Inline errSchema)]
        & required .~ ["error"]
      errSchema = mempty
        & type_ ?~ SwaggerObject
        & properties .~ [("code", Inline codesSchema)
                        ,("message", Inline text)]
      errSchemas = allErrSchemas (Proxy @fmt) (Proxy @es) (Proxy @es)
      codesSchema = mempty
         & type_ ?~ SwaggerString
         & paramSchema.enum_ .~ notEmpty (concat (errSchemas ^.. folded._2))
      notEmpty xs = if null xs then Nothing else Just xs
      text = mempty & type_ ?~ SwaggerString
      examples = TL.toStrict . BL.toLazyText
               . ("Invalid input\n\n" <>)
               . foldr toPretty mempty
               $ concatMap (view _3) errSchemas
      toPretty (txt, val) acc =
        mbHead txt
        <> "```\n"
        <> encodePrettyToTextBuilder val
        <> "\n```\n\n" <> acc
      mbHead txt = if Text.null txt then mempty
                   else "###" <> BL.fromText txt <> "###\n\n"

